//
//  main.m
//  p03-awate
//
//  Created by Shivani Awate on 2/18/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
