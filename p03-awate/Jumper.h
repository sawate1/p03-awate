//
//  Jumper.h
//  p03-awate
//
//  Created by Shivani Awate on 2/26/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy;  // Velocity
@end

