//
//  ViewController.h
//  p03-awate
//
//  Created by Shivani Awate on 2/18/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;


@end
